SMTP-Bruteforcer

## DESCRIPTION
This is a simple bruteforce script using Python. It by default selects the Gmail SMTP server, but when running the script
there is an option to enter a custom SMTP server. For ease of use, I have included the darkc0de wordlist for the bruteforce
that the script will automatically use. If you want to use your own wordlist, place the file in the /res/ folder AND name it
"darkc0de.lst" (Delete the old one).

## INSTALLATION
You need to have the latest version of Python to use this script.

## RUNNING
Open a Terminal window or Command Prompt window and navigate to this folder. Next run "python main.py". This will start the script.

## CONTACT ME
Any questions? Email me at: parth@scapter.org

## READ THIS
I DO NOT TAKE ANY LIABILITY FOR ANY ABUSE USING THIS SCRIPT. 
HACKING INTO ACCOUNTS CAN BE A CRIME. 
I recommend you to NOT use this script to actually hack an email.
Hack to learn, don't learn to hack